import static spark.Spark.*;

public class HelloWorld {

    public static void main(String[] args) {

        get("/hello", (req, res) ->	"Hello World!");

        get("/info", (req, res) -> {
        	return "<a href='http://sparkjava.com/'>Naar de site!</a>";
        });

    }

}