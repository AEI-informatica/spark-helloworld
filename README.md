# Spark HelloWorld
This is an axample of one possible way of structuring a Spark application

See also http://sparkjava.com/.

## Make it work
To run the application on your machine:

```
cd helloworld
mvn clean compile assembly:single
java -jar .\target\helloworld-1.0-jar-with-dependencies.jar
```

Then go to http://localhost:4567/hello and http://localhost:4567/info.

## Docker
The Dockerfile is aimed at a RaspberryPi architecture. Change the first line to
match your architecture. See 

When you have docker installed: 

```
docker build -t robin/java-hello https://rschellius@bitbucket.org/AEI-informatica/spark-helloworld.git
docker run -p 8082:4567 -d robin/java-hello	// run as deamon on external port 8082
docker ps -a 								// shows the container ID
docker exec -it <container ID> /bin/bash	// connects a shell
```

## Critique welcome
If you find anything you disagree with, please feel free to create an issue.

